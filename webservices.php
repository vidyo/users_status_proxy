<?php

require_once 'conf.php';
require_once 'auth.php';

// return the number of users with a specific status
function getStatus(){
	global $db, $db_users;
	
	// get distinct status
	$status = $db->command(array("distinct" => "users", "key" => "status"));
	
	// save the number of users with a specific status
	$result = array();
	foreach ($status['values'] as $st) {
		$result[$st] = $db_users->find(array("status" => $st))->count();
	}
	
	// convert the array to a string
	return serialize($result);
}

// retrieve all the users and for all of them set the status (not the Offline)
function refetchStatus(){
	global $db_users, $USER_API_CREDENTIALS;

	// Create the Soap Client
	$client = new SoapClient(
							$USER_API_CREDENTIALS['url'],
							array(
								'login' => $USER_API_CREDENTIALS['username'],
								'password' => $USER_API_CREDENTIALS['password']
								)
							)
						or die("ERROR: refetchStatus() -> Unable to create soap client!");
	
	$entities = null;
	try {
		// get the total number of members
		$result = $client->search( array('Filter' => array('start' => 0, 'limit' => 0)) );
		// get the total
		$total = $result->total;
		// query with the limit to $total to retreive all the members
		$result = $client->search( array('Filter' => array('start' => 0, 'limit' => $total)) );
		$entities = $result->Entity;
	} catch (SoapFault $fault) {
		die("ERROR: refetchStatus() -> error using Search API: ".$fault->faultcode." - ".$fault->faultstring);
	}
	
	if ($entities != null){
		
		foreach ($entities as $entity){
			$userID = intval($entity->entityID);
			
			// find the user in the db
			$user = $db_users->findOne(array("userID" => $userID));
			// if not found, do not insert it.
			// Users not registered as members (guest, rooms, legacies) needs to disconnect and riconnect to retrieve its staus
			if ($user != NULL){
				// save or insert the current status of each member
				$user['status'] = $entity->MemberStatus;
					
				$db_users->update(array("userID" => $userID), $user);
			}
		}
		
	}

	// if success, return OK in Vidyo style
	return "OK";
}

/***********************************************************
 * DATABASE CONNECTION
 **********************************************************/
// connect
$db_conn = new Mongo();
// select the database
$db = $db_conn->vidyo_cache;

/*
 *  select the USERS collection, NOT the MEMBERS
*  USERS: members, rooms and other devices
*  MEMBERS: just members
*/
$db_users = $db->users;

/***********************************************************
* SOAP SERVER
**********************************************************/

/* specific to php version 5.2.2
 if (!isset($HTTP_RAW_POST_DATA)){
$HTTP_RAW_POST_DATA = file_get_contents('php://input');
}
*/

// check username and password with Basic Authentication
basic_auth();

// Disables the local cache; Use during development.
// Should be turned on for production otherwise  might impact performance
ini_set("soap.wsdl_cache_enabled", "1");

// Creates the SOAP Server
$server = new SoapServer("UsersStatusService.wsdl");
 
// Adds the SOAP Function
$server->addFunction("getStatus");
$server->addFunction("refetchStatus");
 
// Handles a SOAP request
$server->handle();

// check for get params, in case of manual intervention
if (isset($_GET) && isset($_GET['action']) && ($_GET['action'] == 'get_status'))
	echo getStatus();
if (isset($_GET) && isset($_GET['action']) && ($_GET['action'] == 'refetch_status'))
	echo refetchStatus();

?>
