<?php

$realm = 'Restricted web services';

function basic_auth(){
	global $AUTHORIZED_USERS, $realm;
	
	if (!isset($_SERVER['PHP_AUTH_USER'])) {
		header('WWW-Authenticate: Basic realm="'.$realm);
		header('HTTP/1.0 401 Unauthorized');
		die('Request cancelled');
	} else {
		$username = $_SERVER['PHP_AUTH_USER'];
		if ( !isset($AUTHORIZED_USERS[$username]) )
			die('Wrong Credentials!');
		
		if ($_SERVER['PHP_AUTH_PW'] == $AUTHORIZED_USERS[$username] )
			return true;
		else
			die('Wrong Credentials!');
	}
}

function digest_auth(){
	global $AUTHORIZED_USERS, $realm;
	
	if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
	    header('HTTP/1.1 401 Unauthorized');
	    header('WWW-Authenticate: Digest realm="'.$realm.'",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');
	    die('Request cancelled');
	}
	
	
	// analyze the PHP_AUTH_DIGEST variable
	if ( !($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) || !isset($AUTHORIZED_USERS[$data['login']]) )
		die('Wrong Credentials!');
	
	// generate the valid response
	$A1 = md5($data['login'] . ':' . $realm . ':' . $AUTHORIZED_USERS[$data['login']]);
	$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
	$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);
	
	if ($data['response'] != $valid_response)
		die('Wrong Credentials!');
	
	return true;
}

// function to parse the http auth header
function http_digest_parse($txt)
{
    // protect against missing data
    $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
    $data = array();
    $keys = implode('|', array_keys($needed_parts));

    preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

    foreach ($matches as $m) {
        $data[$m[1]] = $m[3] ? $m[3] : $m[4];
        unset($needed_parts[$m[1]]);
    }

    return $needed_parts ? false : $data;
}
?>