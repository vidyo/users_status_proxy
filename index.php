<?php

require_once 'conf.php';
require_once 'auth.php';

function notifyUserStatus($param){
	global $db, $db_users;
	
	if (array_key_exists('UserStatus', $param)){
		// fetch the new user status
		$userStatus = $param->UserStatus;

		// retrieve username and status
		$username = $userStatus->username;
		$memberStatus = $userStatus->MemberStatus;
		
		// insert ot update the user's status in the database
		$db_users->save(array("_id" => $username, "status" => $memberStatus));
		
		// check if the user is a member or a guest
		$member = $db->members->findOne(array("_id" => $username));
		
		if ( ($memberStatus == 'Offline') && ($member == NULL) )
			// remove the guest or other (not member), otherwise all the guests are always added to the database and never deleted
			$db_users->remove(array("_id" => $username));
	}
}

/***********************************************************
 * DATABASE CONNECTION
 **********************************************************/
// connect
$db_conn = new Mongo();
// select the database
$db = $db_conn->vidyo_cache;

/*
 *  use the USERS collection, NOT the MEMBERS
*  USERS: members, rooms and other devices
*  MEMBERS: just members
*/
$db_users = $db->users;

/***********************************************************
* SOAP SERVER
**********************************************************/

/* specific to php version 5.2.2
 if (!isset($HTTP_RAW_POST_DATA)){
$HTTP_RAW_POST_DATA = file_get_contents('php://input');
}
*/

// check username and password with Basic Authentication
basic_auth();

// Disables the local cache; Use during development.
// Should be turned on for production otherwise  might impact performance
ini_set("soap.wsdl_cache_enabled", "1");

// Creates the SOAP Server
$server = new SoapServer("StatusNotificationService.wsdl");
 
// Adds the SOAP Function
$server->addFunction("notifyUserStatus");
 
// Handles a SOAP request
$server->handle();

?>
